// counts Fibonacci
function fibonacci(value){
	if(value < 2){
		return value;
	}
	return fibonacci(value-1)+fibonacci(value-2);	
}

// gets value using prompt; outputs result
function getValue(){
	document.getElementById('result').innerHTML = '';
	let value = 0;
	do{
		value = prompt('Enter value:',value);
	} while(isNaN(value) || value == null);
	value = parseInt(value);
	if(value>0){
		for(let i=0;i<=value;i++){
			document.getElementById('result').innerHTML += fibonacci(i) + ' ';					
		}
	} else {
		for(let i=value;i<=0;i++){
			let koef = (-1)**((-1)*i+1);
			document.getElementById('result').innerHTML += koef * fibonacci((-1)*i) + ' ';					
		}
	}
}