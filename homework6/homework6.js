
// gets values using prompt
function getValue(valueName){
	let value = '';
	do{
		value = prompt('Enter '+valueName+':',value);
	} while(!/^[a-zA-Z0-9_ ]+$/.test(value) || value == '' || value == null);
	return value;
}
 
 // creates user
function createNewUser(){
	let name = getValue('firstName');
	let fam = getValue('lastName');
	
	let newUser = new User();
	newUser.firstName = name;
	newUser.lastName = fam;

	return newUser;
}

// outputs user's login
function getUserLogin(){
	
	let user = createNewUser();	
	document.getElementById('login').innerHTML = user.getLogin();		
	
}


// User's description
function User(){
	let firstName = '';
	let lastName = '';

	Object.defineProperty(this, 'firstName',{
		set: function(value){
			firstName = value;
		},		
		get: function(){
			return firstName;
		}
	});
	
	Object.defineProperty(this, 'lastName',{

		get: function(){
			return lastName;
		},
		set: function(value){
			lastName = value;
		}
	});
}

// function in prototype of User - getLogin - gets login in a given rule
User.prototype.getLogin = function(){
			return (this.firstName[0]+this.lastName).toLowerCase();
		}

