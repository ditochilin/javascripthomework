// parses object into JSON-object; outputs result of cloning
function makeClone(){
	let givenObj = JSON.parse(document.getElementById('given').innerHTML);
	document.getElementById('result').innerHTML = JSON.stringify(clone(givenObj));
}


//  does cloning of object
function clone(givenObj){

	var copy;
	
	if(givenObj == null || 'object' != typeof givenObj){
		return givenObj;
	}

	if(givenObj instanceof Date){
		copy = new Date();
		copy.setTime(givenObj.getTime());
		return copy;
	}

	if(Array.isArray(givenObj)){
		copy = [];
		for(let i=0;i<givenObj.length;i++){
			copy[i] = clone(givenObj[i]);
		}
		return copy;
	}

	if(givenObj instanceof Object){
		copy = {};
		for(let prop in givenObj){
			if(givenObj.hasOwnProperty(prop)){
				copy[prop] = clone(givenObj[prop]);
			}
		}
		return copy;
	}
	throw new Error('Unable to clone given object!');
}