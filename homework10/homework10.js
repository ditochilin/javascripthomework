﻿// enter point; gets given params
function check(){
	let team = document.getElementById('team').value.split(' ');
	let backlog = document.getElementById('backlog').value.split(' ');
	let deadLine = document.getElementById('deadline').valueAsDate;

	let daysRequired = 0;
	let currDay = new Date();
	let sumTeam = sumElements(team);

	while(sumElements(backlog) > 0){
		let tempSumTeam = sumTeam;
		while(tempSumTeam > 0){
			let minStory = Math.min(tempSumTeam, backlog[0]);
   			backlog[0] -= minStory;
			tempSumTeam -= minStory;
			if(backlog[0] == 0 ){
				backlog.shift();
			}
			if(backlog.length == 0 ){
				break;
			}
// 			daysRequired++;
		}
 			daysRequired++;
	}

	distributeDays(currDay, deadLine, daysRequired);
}

// distrubutes days during calendar days, considering if thay are holiday/working
function distributeDays(currDay, deadLine, daysRequired){
	let daysLeft = 0;
	while(daysRequired > 0){
		let weekDay = currDay.getDay();
		if(weekDay > 0 && weekDay < 6){
			daysRequired--;
			daysLeft = Math.floor((deadLine - currDay)/1000/3600/24);
		}
		currDay.setDate(currDay.getDate()+1);
	}

	if(daysLeft > 0){
		alert('успеют за '+daysLeft+' дней до срока.');
	} else if(daysLeft < 0){
		alert('Не успеют на '+(-1)*daysLeft+' дней.');
	} else {
		alert('успеют в срок.');
	}	
}

// gets the suma of array (mas)
function sumElements(mas){
	let sum = 0;
	mas.forEach((elem)=>{sum+=parseInt(elem)});
	return sum;
}