﻿// gets date of birth;  outputs results
function getBirthDate(){
	let parts = prompt('Ваша дата рождения?','dd.mm.yyyy').split(".");
	let birthDate = new Date(parts[2],parseInt(parts[1])-1,parts[0]);
	if(isNaN(birthDate)){
		alert('incorrect Date');
		return;
	}

	alert("Вам "+countAge(birthDate)+" лет!");
	alert('Ваш знак зодиака: '+ getZodiak(birthDate));
	document.getElementById('result').innerHTML = getChiniseYear(birthDate.getFullYear());	
}

// returns a year's name by chinise calendar
function getChiniseYear(birthYear){
	let year = (birthYear - 4) % 12;
	switch(year){
		case 0 : return 'Крыса';
		case 1 : return 'Бык';
		case 2 : return 'Тигр';

		case 3 : return 'Кролик';
		case 4 : return 'Дракон';
		case 5 : return 'Змея';
		case 6 : return 'Лошадь';
		case 7 : return 'Коза';
		case 8 : return 'Обезьяна';
		case 9 : return 'Петух';
		case 10 : return 'Собака';
		case 11 : return 'Свинья';
		default : return '';
	}
}

// counts age for today
function countAge(birthDate){

	let now = new Date();
	let nowYear = now.getFullYear();
	let nowMonth = now.getMonth();
	let nowDay = now.getDate();

	let birthYear = birthDate.getFullYear();
	let birthMonth = birthDate.getMonth();
	let birthDay = birthDate.getDate();

	let years = nowYear - birthYear;
	let months = nowMonth - birthMonth;

	if(months > 0){
		return years;
	} else if(months < 0){
		return years - 1;
	} else {
		let days = nowDay - birthDay;
		if(days > 0){
			return years;
		} else {
			return years - 1;
		}
	}
}

// returns zodiak by birhdate
function getZodiak(birthDate){
	let birthMonth = birthDate.getMonth();
	let birthDay = birthDate.getDate();
	let birthDate01 = new Date(1,birthMonth, birthDay);
	if(isBetween(21,02,19,03,birthDate01)){
		return 'Овен';
	}
	if(isBetween(20,03,20,04,birthDate01)){
		return 'Телец';
	}
	if(isBetween(21,04,21,05,birthDate01)){
		return 'Близнецы';
	}

	if(isBetween(22,05,22,06,birthDate01)){
		return 'Рак';
	}
	if(isBetween(23,06,22,07,birthDate01)){
		return 'Лев';
	}
	if(isBetween(23,07,22,08,birthDate01)){
		return 'Дева';
	}
	if(isBetween(23,08,23,09,birthDate01)){
		return 'Весы';
	}
	if(isBetween(24,09,22,10,birthDate01)){
		return 'Скорпион';
	}
	if(isBetween(23,10,21,11,birthDate01)){
		return 'Стрелец';
	}

	if(isBetween(22,11,19,00,birthDate01)){
		return 'Козерог';
	}
	if(isBetween(20,00,18,01,birthDate01)){
		return 'Водолей';
	}
	if(isBetween(19,01,20,02,birthDate01)){
		return 'Рыбы';
	}
}

// defines if date (testDate) is between given dates (1 and 2)
function isBetween(day1, month1, day2, month2, testDate){
	return new Date(1,month1,day1) <= testDate && new Date(1,month2,day2) >= testDate;
}