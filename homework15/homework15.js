
let min = document.getElementById('minutes');
let sec = document.getElementById('seconds');
let milsec = document.getElementById('milliseconds');

let base = 60; 
let timer = undefined;
let dateStart = undefined;
let dm=0,ds=0,ms=0; 
let h=1,m=1,tm=1,s=0,ts=0,init=0; 

let deltam = 0, deltas = 0, deltams = 0; 

//clears timer and working params
function ClearСlock() { 
	clearTimeout(timer); 

	// setup shift of time when paused
	if(init == 2){
		deltam = tm;
		deltas = ts;
		deltams = ms;
	} else {
		outputTime('00','00','00');	
		deltam = 0;
		deltas = 0;
		deltams = 0;

	h=1;m=1;tm=1;
	s=0;ts=0;ms=0; 
	}

} 

//start timer
function onStartTimer() { 
	var currDate = new Date(); 
	var t = (currDate.getTime() - dateStart.getTime())-(s*1000); 
	
	if (t>999) { 
		s++; 
	} 
	
	if (s>=(m*base)) { 
		ts=0; 
		m++; 
	} else { 
		ts=parseInt((ms/100)+s); 
		if(ts>=base) { ts=ts-((m-1)*base); } 
	} 
	
	if (m>(h*base)) { 
		tm=1; 
		h++; 
	} else { 
		tm=parseInt((ms/100)+m); 
		if(tm>=base) { tm=tm-((h-1)*base); } 
	} 
	
	ms = Math.round(t/10); 
	
	if (ms>99) {ms=0;} 
	if (ms==0) {ms='00';} 
	if (ms>0&&ms<=9) { ms = '0'+ms; } 

	if (ts>0) { 
		ds = ts; 
		if (ts<10) { 
			ds = '0'+ts; 
		}
	} else { 
		ds = '00'; 
	} 
	
	dm=tm-1; 
	if (dm>0) { 
		if (dm<10) { 
			dm = '0'+dm; 
		}
	} else { 
		dm = '00'; 
	} 
	
	outputTime(dm,ds,ms);
	timer = setTimeout("onStartTimer()",1); 
} 

// putputs timer data
function outputTime(minValue, secValue, milsecValue){
	min.innerHTML = minValue;
	sec.innerHTML = secValue;
	milsec.innerHTML = milsecValue;
}

// start handler
function onStart() { 
	if (init != 1){ 
		ClearСlock();
		console.log(deltam);
		dateStart = correctDataStartWithShift(new Date());
		onStartTimer(); 
		init = 1; 
		document.getElementById('start').value = 'Pause';
	} else { 
		clearTimeout(timer);
		init = 2;
		document.getElementById('start').value = 'Start';
	} 
} 

// shifts start date acording to delta(s) when paused
function correctDataStartWithShift(dateStart){
	if(init == 2){
		return (new Date(dateStart.getTime()-(10*deltams + 1000*deltas + 60000*(deltam-1)
											 )
						)
			   );	
	}
	return dateStart;
}

function onClear(){
	ClearСlock();
	init = 0;
	document.getElementById('start').value = 'Start';
}

window.onload = ()=>{
	outputTime('00','00','00');
}