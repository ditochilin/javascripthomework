﻿	let divtimer = document.getElementById('timer');
	let newTimer = new Timer(divtimer);
	let idTimer = undefined;

	let squires = new Map();
	let bombs = new Set();
	let flagedBombs = new Set();
	let gameOvered = false;
	const sideSquire = 40;

let arrayImgsName = [
	'closed', 'bomb', 'nobomb', 'bombed', 'flaged', 'inform', 'zero',
    'num1', 'num2', 'num3', 'num4', 'num5', 'num6', 'num7', 'num8'
];

let mapImgTags = new Map(); 

arrayImgsName.forEach((imgName)=>{
		let imgTag = document.createElement('img');
		imgTag.setAttribute('src', 'img/'+imgName+'.png');
		imgTag.setAttribute('id', imgName);
		imgTag.setAttribute('width', sideSquire);
		imgTag.setAttribute('height', sideSquire);
	
		mapImgTags.set(imgName, imgTag);
});

// compares sets for equality		
function eqSet(aSet, bSet) {
    if (aSet.size !== bSet.size) return false;
    for (let a of aSet) {
    	if (!bSet.has(a)) return false;
    }	
    return true;
}

// Timer object
function Timer(ouputObj){

	this.start = ()=>{
		this.time = 0;
		return setInterval(this.tick,100);	
	}
	this.stop = (idTimer)=>{
		clearInterval(idTimer);
		idTimer = undefined;
	}
	this.tick = ()=>{
		this.time++;
		ouputObj.innerHTML = this.time;
	}
}

// handler for changing size
function onChangeSize(){
	let numSquires = document.getElementById('numSquires');
	numSquires.innerHTML = document.getElementById('numSquiresRange').value;
}

function getNumSquires(){
	return Number(document.getElementById('numSquires').innerHTML);
}

function startNewGame(){
	gameOvered = false;
	resetTimer();
	resetField();
	setStatistic();
}

function resetTimer(){
	if(idTimer != undefined){
		newTimer.stop(idTimer);		
	}

	idTimer = newTimer.start();	
}

// any click handler
function onCanvasClick(evemt){
	if(gameOvered){
		return;
	}
	let pickParams = getPickParams();	
	drawPickedSquire(pickParams.context, 'left',squires.get(getPickedSquireIndex(pickParams.X, pickParams.Y)));		
}

// right button click handler
function onCanvasRightClick(evemt){
	event.preventDefault();
	if(gameOvered){
		return;
	}
	let pickParams = getPickParams();	
	drawPickedSquire(pickParams.context, 'right',squires.get(getPickedSquireIndex(pickParams.X, pickParams.Y)));	
}

// gets prarams of mouse position above canvas; cancas's contet
function getPickParams(){
	let canvas = document.getElementById('canvas');
	let context = canvas.getContext("2d");
	let X = event.clientX - canvas.offsetLeft;
	let Y = event.clientY - canvas.offsetTop;
	return {'context' : context, 'X':X, 'Y':Y};
}

function isBombed(squire){
	return bombs.has(squire.index);
}

// draws squire in an appropriate way;
// event - type of click (left/right/two) button
function drawPickedSquire(context, eventName, pickedSquire){
	
	if(!['flaged','inform','closed'].includes(pickedSquire.state)){
		return;
	}

	if(pickedSquire.state == 'flaged'){
		flagedBombs.delete(pickedSquire.index);
		checkFlagedInform(context, pickedSquire, 'inform');
		return;
	}
	if(pickedSquire.state == 'inform'){
		checkFlagedInform(context, pickedSquire, 'closed');
		return;
	}
	if(eventName=='left'){
		if(isBombed(pickedSquire)){
			gameOver(context);
		} else {
			countSurrounding(context, pickedSquire);	
		}
	} else if(eventName=='right'){
		flagedBombs.add(pickedSquire.index);
		pickedSquire.draw(context,'flaged');
	}

	checkGame();
}

// redraw picked squire, when it is flaged/inform
function checkFlagedInform(context, pickedSquire, state){
	pickedSquire.draw(context, state);
	checkGame();
}

// checks state of the game; refresh statistic
function checkGame(){

	setStatistic();

	if(eqSet(bombs, flagedBombs) && noClosedLeft()){
		newTimer.stop(idTimer);
		gameOvered = true;
		alert('Вы выиграли!');
	}
}

// determines if there is any closed/inform squires
function noClosedLeft(){
	return 	![...squires.values()].some((squire)=>{
		return squire.state === 'closed' || squire.state === 'inform';
	});
}

// sets statistic in DOM's elements
function setStatistic(){
	document.getElementById('flaged').innerHTML = flagedBombs.size;
	document.getElementById('bombsAmount').innerHTML = bombs.size;	
}

// stops game; opens all squires; stops timer.
function gameOver(context){
	newTimer.stop(idTimer);
	squires.forEach((squire)=>{
		if(squire.state!='closed' && squire.state!='flaged'){
			return;
		}

		let flaged = squire.state == 'flaged';
		if(isBombed(squire)){
			if(flaged){
				squire.draw(context, 'bomb');
			} else {
				squire.draw(context, 'bombed');
			}		

		} else {
			if(flaged){
				squire.draw(context, 'nobomb');
			} else {
				countSurrounding(context, squire);				
			}
		}
	});
	gameOvered = true;
	alert('Вы проиграли(!');
}


// checks all ( 8 squires) surrounded squires on bombs 
function getBorderedstate(pickedSquire){
	let numSquires = getNumSquires();
	let countBombs = 0;
	
	let index = pickedSquire.index;
	let indexHasUpperNeighbours = (index % numSquires) != 0;
	let indexHasLowerNeighbours = ((index+1) % numSquires) != 0;
	
		if(index >= numSquires){// all that have left neighbours	
			// check left squire
			countBombs = checkOneBorderedSquire(index - numSquires, countBombs);
			// check upper squire left
			if(indexHasUpperNeighbours){
				countBombs = checkOneBorderedSquire(index - numSquires - 1, countBombs);
			}

			// check lower squire left
			if(indexHasLowerNeighbours){
				countBombs = checkOneBorderedSquire(index - numSquires + 1, countBombs);
			}
		}
		
		// check upper squire
		if(indexHasUpperNeighbours){
			countBombs = checkOneBorderedSquire(index - 1, countBombs);
		}
		// check lower squire
		if(indexHasLowerNeighbours){
			countBombs = checkOneBorderedSquire(index + 1, countBombs);
		}

		if(index < (numSquires - 1) * numSquires){  // all that have right neighbours
			// check right squire
			countBombs = checkOneBorderedSquire(index + numSquires, countBombs);
			// check upper squire right
			if(indexHasUpperNeighbours){
				countBombs = checkOneBorderedSquire(index + numSquires - 1, countBombs);
			}
			// check lower squire right
			if(indexHasLowerNeighbours){
				countBombs = checkOneBorderedSquire(index + numSquires + 1, countBombs);
			}
		}

	return arrayImgsName[countBombs + 6];
}

function checkOneBorderedSquire(index, countBombs){
	try{
		let neighbor = squires.get(index);
		if(!isBombed(neighbor)){
			return countBombs;	
		}
		
		return ++countBombs;
	} catch (ex){
		// out of boundery exception nothing todo
		return countBombs;
	}
	
}

// checks surrounded squires; 
// shows number of bordered bombs
function countSurrounding(context, pickedSquire){
	if(pickedSquire.state != 'closed'){
		return;
	}
	let borderedstate = getBorderedstate(pickedSquire);
	pickedSquire.draw(context, borderedstate);

	if(borderedstate == 'zero'){
		let numSquires = getNumSquires();
		let index = pickedSquire.index;
		let indexHasUpperNeighbours = (index % numSquires) != 0;
		let indexHasLowerNeighbours = ((index+1) % numSquires) != 0;

		if(index >= numSquires){// all that have left neighbours	
			// check left squire
			countSurrounding(context, squires.get(index - numSquires));
			// check upper squire left
			if(indexHasUpperNeighbours){
				countSurrounding(context, squires.get(index - numSquires - 1));
			}

			// check lower squire left
			if(indexHasLowerNeighbours){
				countSurrounding(context, squires.get(index - numSquires + 1));
			}
		}
		
		// check upper squire
		if(indexHasUpperNeighbours){
			countSurrounding(context, squires.get(index - 1));
		}
		// check lower squire
		if(indexHasLowerNeighbours){
			countSurrounding(context, squires.get(index + 1));
		}

		if(index < (numSquires - 1) * numSquires){  // all that have right neighbours
			// check right squire
			countSurrounding(context, squires.get(index + numSquires));
			// check upper squire right
			if(indexHasUpperNeighbours){
				countSurrounding(context, squires.get(index + numSquires - 1));
			}
			// check lower squire right
			if(indexHasLowerNeighbours){
				countSurrounding(context, squires.get(index + numSquires + 1));
			}
		}
	}
}

// find picked squire's index in squires map
function getPickedSquireIndex(x, y){
	let numSquires = getNumSquires();
	let pickedSqX = Math.floor(x / sideSquire);
	let pickedSqY = Math.floor(y / sideSquire);
	return numSquires * pickedSqX + pickedSqY;
}

function resetField(){
	let numSquires = getNumSquires();
	flagedBombs.clear();
	generateBombs(numSquires);
	let context = getPreparedCanvas(numSquires*sideSquire, numSquires*sideSquire).getContext("2d");
	
	for (let i = 0; i < numSquires * numSquires; i++) {
		let topX = sideSquire * Math.floor(i/numSquires);   
		let topY = sideSquire *(i % numSquires);
		let squire = new Squire(topX, topY, sideSquire, i);
		squire.draw(context, 'closed');

		squires.set(i, squire);
	}
}

function generateBombs(numSquires){
	bombs.clear();
	// bombs.add(17);
	// bombs.add(4);
	// bombs.add(9);
	// bombs.add(12);

	let numBombs = Math.floor(numSquires * numSquires / 6);
	while(numBombs-- > 0){
		let currBomb = undefined;
		do{
			currBomb = getRandomOfRange(1, numSquires * numSquires);
		}while(bombs.has(currBomb));

		bombs.add(currBomb);
	}
}

function getRandomOfRange(min,max){
	return Math.floor(Math.random() * (max - min)) + min;
}

// gets cleaned canvas 
function getPreparedCanvas(width, height){
	let canvas = document.getElementById('canvas');
	canvas.setAttribute('width', width);
	canvas.setAttribute('height', height);
	canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
	return canvas;
}

function Squire(x, y, size, index){
	this.X = x;
	this.Y = y;

	this.size = size;
	this.index = index;

	this.draw = (context, state)=>{
		this.state = state;
		let img = mapImgTags.get(state);
		context.clearRect(this.X, this.Y, size, size);
    	context.drawImage(img, this.X, this.Y, size, size);    	
	}
}

window.onload = onChangeSize();
