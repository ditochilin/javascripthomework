
// recursive function for counting
function countFuctorial(value){
	if(value === 0){
		return 1;
	}
	return value * countFuctorial(value - 1);
}

// gets a value for counting Factorial using prompt 
function getValue(valueName){
	let value = 0;
	do{
		value = prompt('Enter '+valueName+' value:',value);
	} while(isNaN(value) || value <= 1 || value == null);
	return parseInt(value);
}

// enter piont 
function getSimpleValues(){	
	let resultp = document.getElementById('result');	
	resultp.innerHTML = countFuctorial(getValue());
}
