
// enter point; handle input data; outputs result
function find(){
	let objects = JSON.parse(document.getElementById('objects').innerHTML);	
	let keys = document.getElementById('keys').value.split(' ');
	let searchAll = document.getElementById('searchAll').checked;
	let where = document.getElementById('where').value.split(' ');

	document.getElementById('result').innerHTML = JSON.stringify(filterCollection(objects,keys,searchAll,where));	
	 
}

// filters collection by given options
function filterCollection(objects,keys,searchAll,where){
	let result = [];
	for(let obj of objects){
		if(searchInProps(obj, keys, searchAll, where)){
			result.push(obj);
		}
	}
	return result;	
}

// searches in object (where) propertis of obj by key words (keys)
function searchInProps(obj, keys, searchAll, where){
	for(let prop of where){
		let objPropValue = getObjProp(obj, prop);
		if(objPropValue != null){
			if(hasWordsInProp(objPropValue, keys, searchAll)){
				return true;
			}
		}
	}
	return false;
}

// gets prop value of object (obj)
function getObjProp(obj, prop){
	let props = prop.split('.');
	let value = obj;
	return getPropValue(getStringOfProp(value,props),props);
}

// gets value of property by value
function getPropValue(value,props){
	if(props.length == 0){
		return value;
	}
	return getPropValue(getStringOfProp(value,props),props)
}

// from array of properties (props) gets first property and recieves it's value
function getStringOfProp(value, props){
	return value[props.shift().toString()];
}

//determines if the property has (includes) a word (key in keys)
function hasWordsInProp(propVal, keys, searchAll){
	let res = false;
	for(let word of keys){
		if(propVal.toLowerCase().includes(word.toLowerCase())){
			if(searchAll){
				res = true;
				continue;
			} else {
				return true;
			}
		} else if(searchAll){ // if not includes and not all words to search - turn off res
			res = false;
		}
	}
	return res;
}