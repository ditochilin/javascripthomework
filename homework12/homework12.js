﻿// a collection of drawn shapes (circles)
let shapes = [];

// entry point; gets tag for drawing canvas
function drawCircle(event){
	let divContainer = getMainContainer();
	divContainer.innerHTML = '';
	addInputFields(divContainer);

	let canvas = document.createElement('canvas');
	canvas.setAttribute('id', 'canvas');
	canvas.width = 600;
	canvas.height = 600;
	canvas.addEventListener('click',(event)=>{onClickCanvas(event);});
	divContainer.appendChild(canvas);
}

// canvas' on click event handler
function onClickCanvas(event){
	let canvas = document.getElementById('canvas');
	let X = event.clientX - canvas.offsetLeft;
	let Y = event.clientY - canvas.offsetTop;

	let foundShape = shapes.find((shape)=>{
		return (X >= (shape.centreX-20) && (X <= shape.centreX+20)
			&& (Y >=shape.centreY-20) && (Y <= shape.centreY+20));
	});

	changeShapeColor(foundShape, canvas);
}

// changes color of picked shape if it was
function changeShapeColor(foundShape, canvas){
	if(foundShape==undefined){
		return;
	}

	shapes.splice(shapes.indexOf(foundShape),1);

	let shape = new Circle(foundShape.centreX, foundShape.centreY, [21, '#FFFFFF'],canvas);
		shape.draw();
		shapes.push(shape);  
}

// gets main div container
function getMainContainer(){
	return document.getElementById('container');
}

// draws input fields for insertion values: radius & color
function addInputFields(divContainer){
	// radius
	let labelRadius = document.createElement('label');
	labelRadius.innerHTML = 'Radius: ';
	let inputRadius = document.createElement('input');
	inputRadius.setAttribute('type','number');
	inputRadius.setAttribute('id','radius');
	//inputRadius.addEventListener("change", onChangeRadius);

	// color
	let labelColor = document.createElement('label');
	labelColor.innerHTML = 'Color: ';
	let inputColor = document.createElement('input');
	inputColor.setAttribute('type','text');
	inputColor.setAttribute('id','color');
	//inputColor.addEventListener("change", onChangeColor);

	// btn to draw circle
	let btnDraw = document.createElement('button');
	btnDraw.setAttribute('id', 'btnDraw');
	btnDraw.setAttribute('class', 'btn-my');
	btnDraw.setAttribute('onclick', 'draw()');
	btnDraw.innerHTML = 'Нарисовать';

	// btn to draw 100 circles
	let btnDraw100 = document.createElement('button');
	btnDraw100.setAttribute('id', 'btnDraw');
	btnDraw100.setAttribute('class', 'btn-my');
	btnDraw100.setAttribute('onclick', 'draw100()');
	btnDraw100.innerHTML = 'Нарисовать 100 кругов';

	let divPanel = document.createElement('div');
	divContainer.appendChild(divPanel);
	divPanel.appendChild(labelRadius);
	divPanel.appendChild(inputRadius);
	divPanel.appendChild(document.createElement('br'));
	divPanel.appendChild(document.createElement('br'));
	divPanel.appendChild(labelColor);
	divPanel.appendChild(inputColor);
	
	divPanel.appendChild(document.createElement('br'));
	divPanel.appendChild(document.createElement('br'));
	divPanel.appendChild(btnDraw);

	let spanSpace = document.createElement('span');
	spanSpace.style.margin = '30px';
	divPanel.appendChild(spanSpace);
	divPanel.appendChild(btnDraw100);

}

// draws a circle with given radius & color
function draw(){
	let canvas = getPreparedCanvas();
	new Circle(canvas.width/2, canvas.height/2, getCircleParams(),canvas).draw();
}

// draws 100 cirles with random color
function draw100(){
	shapes.length = 0;
	let canvas = getPreparedCanvas();
	for (let i = 0; i < 100; i++) {
		let centreX = 50 * Math.floor(i/10+1);   // 50 - for shift
		let centreY = 50 *(i % 10+1);
		let shape = new Circle(centreX, centreY, [20, getRGB()],canvas);
		shape.draw();
		shapes.push(shape);  
	}
}

// gets RGB in hex format '#rgb'
function getRGB(){
	let r = getRandomOfRangeHEX(0,256);
	let g = getRandomOfRangeHEX(0,256);
	let b = getRandomOfRangeHEX(0,256);

	return '#' + r + g + b;
}

// gets cleaned canvas 
function getPreparedCanvas(){
	let canvas = document.getElementById('canvas');
	canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
	return canvas;
}

// get a random value in range of values [min, max]
function getRandomOfRangeHEX(min, max){
	return (Math.floor(Math.random()*(max - min)) + min).toString(16);
}

// gets array with params: {0} - radius; {1} - color
function getCircleParams(){
	return [
		document.getElementById('radius').value,
		document.getElementById('color').value
	];
}

// build a circle object
function Circle(centreX, centreY, params, canvas){
	
	this.centreX = centreX;
	this.centreY = centreY;
	
	this.draw = () => {
		this.radius = params[0], 
		this.color = params[1];
	
		let context = canvas.getContext('2d');
		context.beginPath();
		context.arc(this.centreX, this.centreY, this.radius, 0, 2 * Math.PI);
		context.fillStyle = this.color;
		context.fill();
	}
}