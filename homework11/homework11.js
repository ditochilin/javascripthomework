﻿let modal = document.getElementById('winItemsNum');
let btnShowSetNumber = document.getElementById('btnShowSetNumber');
let btnSave = document.getElementById('btnSave');
let itemsNumber = document.getElementById('itemsNumber');
let errorLabel = document.getElementById('errorLabel');
let span = document.getElementsByClassName('close')[0];

let modalHeader = document.getElementById('modalHeader');
let itemContent = document.getElementById('itemContent');
let itemsNumberDiv = document.getElementById('itemsNumberDiv');

let itemsNumberVal = 0;
let contexts = [];


itemsNumber.onchange = checkItemsNumber;
span.onclick = hideModal;

// check if itemsNumber is a number - disable/able btn for save value
function checkItemsNumber(){
	 let localItemsNum = parseInt(itemsNumber.value);
	 let isItemsRight = !isNaN(localItemsNum);
	 errorLabel.hidden = isItemsRight;
	 btnSave.disabled = !isItemsRight; 
	 if(isItemsRight){
	 	btnSave.classList.remove('disabled');
	 } else {
	 	btnSave.classList.add('disabled');
	 }
}

// works in two modes: 0 - input items number ; 1 - input context of i's item
// for mode == 0 : save itemsNumber & hides midal window 
// fir mode == 1 : puts value of context in array contexts, shows modal window for next item
function save(){
	let mode = Number(document.getElementById('modalMode').value);
	if(mode === 0){		
		itemsNumberVal = parseInt(itemsNumber.value);
		hideModal();
		makeInputItems();		
	} else if(mode === 1){
		let itemContentVal = `${document.getElementById('itemContentVal').value}`;
		contexts.push(itemContentVal);
		let contextsLen = contexts.length;
		if(contextsLen === itemsNumberVal){
			hideModal();
			buildItems(contexts);
			return;
		}
		itemContentVal = ``;
		showModal(mode,contextsLen+1);
	}
}

// outputs contexts
function buildItems(contents){
	let divListContents = document.getElementById('divListContents');
	//let divContents = document.getElementById('divContents');


	const markup = `
	<ul>
	${contents.map(content => 
		`<li>${content}</li>`
		).join('')}
	</ul>
	`;
	divListContents.innerHTML = markup;

	let newTimer = new Timer(10);
	let interval = setInterval(()=>{
		newTimer.tick(interval);
	}, 1000);
}

//  function to setup, ouput and run timer value
function Timer(time){
	this.time = time;
	this.timerElement = document.getElementById("timer");

	this.tick = interval => {
		this.time--;
		if(this.time <= 0){
			clearInterval(interval);
			this.time = '';
			clearContent();
		} 
		this.timerElement.innerHTML = this.time; 
	}
}

// clear content with items
function clearContent(){
	document.getElementById('divListContents').innerHTML = '';
}

// opens modal window for input item's content
function makeInputItems(){
	if(itemsNumberVal===0){
		alert('Не введено число пунктов!');
		return;
	}

	contexts = [];
	showModal(1, 1);
}

//mode - enter number items; enter iten's contex;
function showModal(mode, i=0){
	modal.style.display = 'block';
	document.getElementById('modalMode').value = mode;
	itemsNumber.value = 0;
	itemContentVal.value = '';
	let isMode0 = mode===0;
	modalHeader.innerHTML = isMode0 ? 'Укажите число пунктов': 'Введите содержание '+i+'-го пункта:';
	itemContent.style.display = isMode0 ? 'none' : 'flex';
	itemsNumberDiv.style.display = isMode0 ? 'flex' : 'none';	
}

//  sets default itemsNumber when modal window opens
function setItemsNumber(){
	showModal(0);
	checkItemsNumber();
}

// sets modal window's mode to hidden
function hideModal(){
	modal.style.display = 'none';
}

// discribes behevior of click outside the modal window
window.onclick = function(event){
	if(event.target === modal){
		hideModal();
	}
}