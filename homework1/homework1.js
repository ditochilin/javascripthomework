
const denyMessage = 'You are not allowed to visit this website.';
const askName = 'Enter your name:';
const askAge = 'Enter your age:';
const askContinue = 'Are ypu sure you want to continue?';

function login(){
	let userName = '';
	do{
		userName = prompt(askName,userName);
	} while(!/^[a-zA-Z0-9_ ]+$/.test(userName) || userName === '');

	let age = 0;
	do{
		age = prompt(askAge,age);
	} while(isNaN(age) || age == 0 || age == null);

	checkAge(userName, parseInt(age));
	
}

function checkAge(userName, age){
		if(age < 18){
			alert(denyMessage);
		}else if(age >= 18 && age <= 22){
			if(confirm(askContinue)){
				alert(getWelcomeMsg(userName));
			} else {
				alert(denyMessage);
			}
		} else {
			alert(getWelcomeMsg(userName));
		}
	
}

function getWelcomeMsg(userName){
	return 'Welcome, '+userName;
}