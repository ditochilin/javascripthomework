// function for recieving (input) values and calling countSimple
function getSimpleValues(){
	let valueM = getValue('min');
	let valueN = getValue('max');

	countSimple(valueM, valueN);
	
}

// gets a value using  prompt function
function getValue(valueName){
	let value = 0;
	do{
		value = prompt('Enter '+valueName+' value:',value);
	} while(isNaN(value) || value <= 1 || value == null);
	return parseInt(value);
}

// counts simple value
function countSimple(valueM, valueN){
	let setOfValues = fulFillSet(valueM, valueN);
	if(setOfValues.size === 0){
		alert('No any simple value!');
		return;
	}
	
	for(let key = 2; key*key <= valueN; key++ ){
		let j = key*key, i = 0;
	
		while(j<=valueN){
			setOfValues.delete(j);
			i++;
			j = key*(key + i);				
		}
	}
	
	let resultp = document.getElementById('result');
	resultp.innerHTML = ''; 
	setOfValues.forEach((value)=>{
		console.log(value);
		resultp.innerHTML += value + ' ';
	});
}

// fulling a set of values between min and max
function fulFillSet(valueM, valueN){
	let setOfValues = new Set();
	for(let i = valueM; i<=valueN;i++){
		setOfValues.add(i);
	}
	return setOfValues;
}