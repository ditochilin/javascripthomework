// enter point;  gets given values (arrays), outputs result of exclusion
function findExcluded(){
	let firstMas = JSON.parse(document.getElementById('firstMas').innerHTML);	
	let secondMas = JSON.parse(document.getElementById('secondMas').innerHTML);
	let prop = document.getElementById('prop').value;
	document.getElementById('result').innerHTML = JSON.stringify(excludBy(firstMas,secondMas,prop));	
	 
}

// defines if second mas has a value of given property
function hasSecondMas(secondMas, prop, currPropValue){
	for(let obj of secondMas){
		if(obj[prop] === currPropValue){
			return true;
		}
	}
	return false;
}

// does exclusion of a value by property
function excludBy(firstMas,secondMas,prop){
	let result = [];
	for(let obj of firstMas){
		let currPropValue = obj[prop];
		if(hasSecondMas(secondMas, prop, currPropValue)){
			continue;
		} else {
			result.push(obj);
		}
	}
	return result;
}